package spy

import (
	"bytes"
	"fmt"
	"gitlab.srv.pv.km/go-libs/observer"
	"gitlab.srv.pv.km/go-libs/utils"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const SPY_INDENT int = 2

var writerLog WriteLog

type WriteLog interface {
	Write(...string)
}

type Spy struct {
	Name      string
	EndMsg    string
	StartTime time.Time
	EndTime   time.Time
	level     int
	context   string
	ended     bool
	children  []*Spy
}

type chan_message struct {
	Severity string
	Message  string
}

var log_chan chan interface{}
var log_writer chan interface{}
var stop_ch chan bool

func init() {
	stop_ch = make(chan bool)
	log_chan = make(chan interface{})
	observer.Subscribe("log", log_chan)
	var data interface{}

	go func() {
		for {
			select {
			case data = <-log_chan:
				message := data.(*chan_message)
				if writerLog != nil {
					writerLog.Write(message.Severity, message.Message)
				}
			}
		}
	}()
}

func InitLog(w WriteLog) {
	writerLog = w
}

func initSpy(context string, name string, endMsg string, level int) *Spy {
	s := &Spy{
		level:     level,
		Name:      name,
		EndMsg:    endMsg,
		StartTime: time.Now(),
		context:   context,
	}
	s.LogDebug(name)
	return s
}

func NewSpy(name string, message ...string) *Spy {
	if len(message) > 0 {
		return initSpy("", name, message[0], 0)
	} else {
		return initSpy("", name, name, 0)
	}
}

func (self *Spy) SubSpy(name string, endMsg ...string) *Spy {
	var s *Spy
	if len(endMsg) > 0 {
		s = initSpy(self.context, name, endMsg[0], self.level+1)
	} else {
		s = initSpy(self.context, name, name, self.level+1)
	}

	self.children = append(self.children, s)
	return s
}

func (self *Spy) SetContext(context string) {
	self.context = context
}

func (self *Spy) End() {
	if self.ended {
		self.LogError("Double Spy.End()", self.context)
	} else {
		self.EndTime = time.Now()
		self.ended = true
		for _, child := range self.children {
			child.level -= 1
			if !child.ended {
				child.End()
			}
		}
		self.LogDebug("%s (%v)", self.EndMsg, self.EndTime.Sub(self.StartTime))
	}
}

func (self *Spy) wrapSpace(format string, values ...interface{}) string {
	return fmt.Sprintf(strings.Repeat(" ", self.level*SPY_INDENT)+" ["+self.context+"] "+format, values...)
}

func (self *Spy) LogInfo(format string, values ...interface{}) {
	self.logInternal(" INFO  ", self.wrapSpace(format, values...))
}

func (self *Spy) LogFatal(format string, values ...interface{}) {
	self.logInternal(" FATAL ", self.wrapSpace(format, values...))
	// log.Fatal(`[FATAL] :: %v`, err.Error())
}

func (self *Spy) LogError(format string, values ...interface{}) {
	self.logInternal(" ERROR ", self.wrapSpace(format, values...))
}

func (self *Spy) LogWarning(format string, values ...interface{}) {
	self.logInternal("WARNING", self.wrapSpace(format, values...))
}

func (self *Spy) LogDebug(format string, values ...interface{}) {
	self.logInternal(" DEBUG ", self.wrapSpace(format, values...))
}

func (self *Spy) logInternal(severity string, msg string) {
	var buffer bytes.Buffer
	if self.context == "" {
		self.context = "nocontext"
	}

	buffer.WriteString(utils.FormatTimeForLog(time.Now()))
	buffer.WriteString(` [` + severity + `]: `)
	buffer.WriteString(msg)

	observer.Publish("log", &chan_message{Message: buffer.String(), Severity: severity})
}

// TODO: create another lib?
// func (self *Spy) SetSpyWriter(w Writer) {
//
// usage:
//
// spy.SetSpyWriter(res, req)
// defer spy.RestoreWriter()
//
func (self *Spy) SetSpyWriter(res http.ResponseWriter, req *http.Request) {
	if req.FormValue("loglevel") == "" {
		return
	}

	lvl := req.FormValue("loglevel")
	l, err := strconv.Atoi(lvl)
	if err != nil {
		self.LogError("Unknown log level: %v", err)
		return
	}
	if l == 2 {
		log_writer = make(chan interface{})
		observer.Subscribe("log", log_writer)

		go func() {
			var data interface{}
			var message *chan_message
			for {
				select {
				case data = <-log_writer:
					if data != nil {
						message = data.(*chan_message)
						res.Write([]byte(message.Message + "\n"))
					}
				case <-stop_ch:
					observer.UnSubscribe("log", log_writer)
					log_writer = nil
					return
				}
			}
		}()
	}
}

func (self *Spy) ResetSpyWriter() {
	if log_writer != nil {
		stop_ch <- true
	}
}
