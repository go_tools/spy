package spy

import (
	"fmt"
	"gitlab.srv.pv.km/go-libs/scribe"
)

type LOG_PRINT struct {
	Scribe *scribe.Scribe
	write  write
}

type write func(message ...string)

func (lp *LOG_PRINT) Write(message ...string) {
	lp.write(message[0], message[1])
}

func SetLogger(level int, config *scribe.ScribeConfig) {
	LOG := &LOG_PRINT{}
	if config != nil {
		LOG.Scribe = scribe.NewScribe(config)
	}

	LOG.write = LOG.writeToStdOut
	InitLog(LOG)

	spy := NewSpy("Init logger", "Logger running")
	defer spy.End()

	if LOG.Scribe != nil {
		go func() {
			for message := range LOG.Scribe.LogMessage {
				spy.LogDebug(message)
			}
		}()
	}

	switch level {
	case 2:
		LOG.write = LOG.writeToScribe_and_StdOut
	case 3:
		LOG.write = LOG.writeToScribe
	default:
		LOG.write = LOG.writeToStdOut
	}

	InitLog(LOG)
}

func (lp *LOG_PRINT) writeToStdOut(message ...string) {
	fmt.Println(message[1])
}

func (lp *LOG_PRINT) writeToScribe_and_StdOut(message ...string) {
	if lp.Scribe.IsConnected() {
		if len(message) == 2 && len(message[0]) > 0 && len(message[1]) > 0 {
			lp.Scribe.Write(message[0], message[1])
		}
	} else {
		fmt.Println(message[1])
	}
}

func (lp *LOG_PRINT) writeToScribe(message ...string) {
	if lp.Scribe.IsConnected() {
		if len(message) == 2 && len(message[0]) > 0 && len(message[1]) > 0 {
			lp.Scribe.Write(message[0], message[1])
		}
	}
}
