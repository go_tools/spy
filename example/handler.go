package main

import (
	"encoding/json"
	"gitlab.srv.pv.km/go-framework/spy"
	"net/http"
	"os"
	"os/signal"
	"fmt"
)

type Handler struct {
	Spy      *spy.Spy
}

type REQUEST struct {
	Status   string      `json:"status"`
	Response interface{} `json:"response"`
}

func CatchSignals(Spy *spy.Spy) {
	c := make(chan os.Signal, 1)
	fmt.Println("CREATE CATCH")
	signal.Notify(c, os.Interrupt)
	go func() {
		select {
		case <-c:
			Spy.End()
			os.Exit(1)
		}
	}()
}

func (self *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	request := &REQUEST{Status: `OK`}

	self.Spy.SetSpyWriter(w, r)
	defer self.Spy.ResetSpyWriter()


	switch r.URL.Path {
	case "/favicon.ico":
		return
	case "/test":
		self.Spy.LogDebug(" Request - test ")
	default:
		// request.Response, err = self.Services.ReadData(r, w)
	}

	self.Spy.LogDebug(" Request - ANTI test ")

	if err != nil {
		request.Status = `Error`
		request.Response = err.Error()
	}

	result, _ := json.Marshal(request)
	w.Write(result)
}