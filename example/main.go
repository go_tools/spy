package main

import (
	"encoding/json"
	"gitlab.srv.pv.km/go-framework/spy"
	"gitlab.srv.pv.km/go-libs/config"
	"gitlab.srv.pv.km/go-libs/scribe"
	"bytes"
	"strconv"
	"net/http"
	"time"
	"log"
)

type ENV_CONFIG struct {
	Http struct {
		Port         int `json:"Port"`
		ReadTimeout  int "json:`ReadTimeout`"
		WriteTimeout int `json:"WriteTimeout"`
	} `json:"http"`
	Services *json.RawMessage `json:"services"`
	Scribe   *json.RawMessage `json:"scribe"`
}

func main() {
	var buffer bytes.Buffer
	const ENV_PATH = "env.json"

	env := &ENV_CONFIG{}
	config.ReadFile(ENV_PATH, env)

	ScribeConfig := &scribe.ScribeConfig{}

	err := json.Unmarshal(*env.Scribe, ScribeConfig)
	if err != nil {
		log.Fatal("Can`t read Scribe config :: ", err.Error())
	}
	spy.SetLogger(1, ScribeConfig)

	Spy := spy.NewSpy("Start application", "End application")
	Spy.SetContext("example")

	timeout := time.Duration(1) * time.Second
	time.Sleep(timeout)

	qwe := `Test`
	Spy.LogDebug("Test message [%s]", qwe)

	subSpy := Spy.SubSpy("Sub spy start", "Sub spy end")
	subSpy.SetContext("Sub_Spy")

	subSpy.LogError("Sub debug")
	subSpy.LogDebug("Sub debug")
	subSpy.End()

	defer Spy.End()

	buffer.WriteString(":")
	buffer.WriteString(strconv.Itoa(env.Http.Port))

	server := &http.Server{
		Addr: buffer.String(),
		Handler: &Handler{
			Spy:      Spy,
		},
		ReadTimeout:  time.Duration(env.Http.ReadTimeout) * time.Second,
		WriteTimeout: time.Duration(env.Http.WriteTimeout) * time.Second,
	}
	server.SetKeepAlivesEnabled(false)
	log.Fatal(server.ListenAndServe())

}